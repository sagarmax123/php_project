<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <form action="form_processing.php" method="get">
        <label for="math">Math&#09</label>
        <input type="number" id="math" name="math"><br><br>
        <label for="english">English</label>
        <input type="number" id="english" name="english"><br><br>
        <label for="bengali">Bengali</label>
        <input type="number" id="bengali" name="bengali"><br><br>
        <label for="physics">Physics</label>
        <input type="number" id="physics" name="physics"><br><br>
        <input type="submit" value="Result">
    </form>
</body>
</html>